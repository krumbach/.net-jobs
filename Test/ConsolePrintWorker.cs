using System;
using Jobs;
using System.Xml.Serialization;
using Common.Utilities.Log;
using System.IO;

namespace Test
{
	public class ConsolePrintWorker : WorkerBase
	{
		public ConsolePrintWorker(ILogger logger) : base(logger)
		{
			// Nothing to do.
		}

		protected override void Execute(IParameters parameters)
		{
			var workerParams = parameters as ConsoleWorkerParams;

			for (int i = 0; i < workerParams.Count; i++) {
				Console.WriteLine (workerParams.Message);
			}
		}
	}

	// Strongly types parameters for the worker.
	//
	public class ConsoleWorkerParams : IParameters
	{
		public string Message { get; set; }
		public int Count { get; set; }

		public string Encode ()
		{
			return SerializeObject (this);
		}

		public void Decode (string objectData)
		{
			var temp = (ConsoleWorkerParams)XmlDeserializeFromString(objectData, typeof(ConsoleWorkerParams));

			this.Count = temp.Count;
			this.Message = temp.Message;
		}

		public static string SerializeObject(object obj)
		{
			XmlSerializer xmlSerializer = new XmlSerializer(obj.GetType());
			StringWriter textWriter = new StringWriter();

			xmlSerializer.Serialize(textWriter, obj);
			return textWriter.ToString();
		}

		public static object XmlDeserializeFromString(string objectData, Type type)
		{
			var serializer = new XmlSerializer(type);
			object result;

			using (TextReader reader = new StringReader(objectData))
			{
				result = serializer.Deserialize(reader);
			}

			return result;
		}
	}
}

