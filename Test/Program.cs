using System;
using Jobs.ServicesImp;
using Common.Utilities.Log;
using System.Threading;

namespace Test
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			// Inject the MSMQ Message Service to use for the queue.
			var msqService = new MsqmService ();

			Jobs.Jobs jobs = new Jobs.Jobs("testing", false, 1, msqService, new DebugLogger());

			// get the fully qualified name of the worker params.
			Console.WriteLine (typeof(ConsoleWorkerParams).AssemblyQualifiedName);

			jobs.Start ();

			// parameters that will be passed to the worker's execute method.
			var workerParams = new ConsoleWorkerParams {
				Message = "Hello World!",
				Count = 5
			};

			jobs.Enqueue (
				"Test.ConsolePrintWorker, Test",  // need fully qualified name of worker
				"Test.ConsoleWorkerParams, Test", // need fully qualified name of the worker parameters
				workerParams.Encode ());

			while (true) {
				Thread.Sleep (10000);

				jobs.Enqueue (
					"Test.ConsolePrintWorker, Test", 
					"Test.ConsoleWorkerParams, Test", 
					workerParams.Encode ());
			} 
		}
	}
}
