﻿namespace Jobs
{
    public class JobsQueueMessage
    {
        public string JobClassName { get; set; }
        public string ParamsClassName { get; set; }
        public string ParameterData { get; set; }
        public int RetryAttempts { get; set; }
    }
}