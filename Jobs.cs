﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Utilities.Log;
using Common.Utilities.StringEx;
using System.Collections.Generic;

namespace Jobs
{
    public class Jobs
    {  
		private readonly IMessageService _service;
		private readonly IMessageQueue _messageQueue;
		private readonly bool _useQueue;
		private readonly int _numberWorkers;

		private readonly ILogger _logger;
        private CancellationTokenSource _cancelSource;
        private Task[] _tasks;

		public Jobs(string name, bool useQueue, int numberWorkers, IMessageService messageService, ILogger logger)
        {
			_service = messageService;
            _logger = logger;
            _useQueue = useQueue;
			_numberWorkers = numberWorkers;

			_messageQueue = _service.CreateQueue(name);
        }

        public void Start()
        {
			if (!_useQueue) {
				return; // no threading.
			}

            // Has it already started?
            //
            if (_cancelSource != null)
            {
                return;
            }

            _cancelSource = new CancellationTokenSource();

            // Start the task.
            //
            try
            {
				var tasks = new List<Task>();

				for(int i = 0; i < _numberWorkers; i++)
				{
					tasks.Add(Task.Factory.StartNew(ProcessMessages));
				}

				_tasks = tasks.ToArray();

            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message, ex);

                throw;
            }
        }

        public void Stop()
        {
            if (_cancelSource != null)
            {
                // Tell the worker to stop.
                //
                _cancelSource.Cancel();

                // Block and wait for all to complete.
                //
                Task.WaitAll(_tasks);
            }

            _cancelSource = null;
        }

        public void Enqueue(string jobClassName, string paramsClassName, string parameterData, int attempts = 3)
        {
            var jobMessage = new JobsQueueMessage
            {
                JobClassName = jobClassName,
                ParamsClassName = paramsClassName,
                ParameterData = parameterData,
                RetryAttempts = attempts
            };

            if (_useQueue)
            {
				_messageQueue.Send(jobMessage);
            }
            else
            {
				ProcessSingleJob(jobMessage);
            }
        }

        private void ProcessMessages()
        {
            while (true)
            {
				var jobMessage = _messageQueue.Recieve(5);

                // Process the message.
                //
				if (jobMessage != null)
                {
					ProcessSingleJob(jobMessage);
                }

                if (_cancelSource.IsCancellationRequested)
                {
                    return;
                }
            }
        }

		private void ProcessSingleJob(JobsQueueMessage jobMessage)
        {
            var type = Type.GetType(jobMessage.JobClassName);
			if (type == null) {
				throw new InvalidCastException("Unable to get type from class name={0}".FormatWith(jobMessage.JobClassName));
			}

            _logger.Debug("Creating job using class={0}".FormatWith(type.Name));

			var worker = (WorkerBase)Activator.CreateInstance(type, new object[] { _logger });
            if (worker == null)
            {
                throw new Exception("Unable to create instance of type={0}".FormatWith(type.Name));
            }

            var paramType = Type.GetType(jobMessage.ParamsClassName);
            if (paramType == null)
            {
                throw new InvalidCastException("Unable to get type from qualified name={0}".FormatWith(jobMessage.ParamsClassName));
            }

            var parameters = (IParameters)Activator.CreateInstance(paramType);
            if (parameters == null)
            {
                throw new Exception("Unable to create instance of type={0}".FormatWith(paramType.Name));
            }

			parameters.Decode(jobMessage.ParameterData);

			worker.Perform(parameters);
        }
    }
}