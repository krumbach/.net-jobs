namespace Jobs
{
	public interface IMessageService
	{
		/// <summary>
		/// If the queue already exists, returns it; otherwise creates the new one.
		/// </summary>
		/// <returns>The queue.</returns>
		/// <param name="name">Name.</param>
		IMessageQueue CreateQueue (string name);
	}
}