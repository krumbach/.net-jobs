using System.Text.RegularExpressions;

namespace Common.Utilities.StringEx
{
	public static class StringExtensionMethods
	{
        public static string FormatWith(this string target, params object[] args)
        {
            return string.Format(target, args);
        } 
	}
}