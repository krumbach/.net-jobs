using Common.Utilities.Log;
using Common.Utilities.StringEx;

namespace Jobs
{
	public abstract class WorkerBase
    {
        private readonly ILogger _logger;

	    protected WorkerBase(ILogger logger)
        {
            _logger = logger;
        }

		protected abstract void Execute (IParameters parameters);

        public void Perform(IParameters parameters)
        {
            _logger.Debug("Start execute of worker on {0}".FormatWith(GetType().Name));

            Execute(parameters);

            _logger.Debug("End execute of worker on {0}".FormatWith(GetType().Name));
        }
    }
}