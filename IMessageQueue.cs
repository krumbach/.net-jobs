namespace Jobs
{
	public interface IMessageQueue
	{
		/// <summary>
		/// Put a JobsQueueMessage on the queue.
		/// </summary>
		/// <param name="obj">Object.</param>
		void Send(JobsQueueMessage obj);

		/// <summary>
		/// Get the next JobsQueueMessage from the queue.
		/// </summary>
		/// <param name="timeOutSeconds">Time out seconds.</param>
		JobsQueueMessage Recieve(int timeOutSeconds);
	}
}