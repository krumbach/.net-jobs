using System;
using System.Messaging;
using Common.Utilities.StringEx;

namespace Jobs.ServicesImp
{
	public class MsqmService : IMessageService
	{
		public IMessageQueue CreateQueue (string name)
		{
			var messageQueue = MessageQueue.Exists(name) ? new MessageQueue(name) : MessageQueue.Create(name);

			if (messageQueue == null) {
				throw new NullReferenceException ("Unable to create the queue named={0}".FormatWith (name));
			}

			// Set the formatter for our internal jobs object message type.
			//
			messageQueue.Formatter = new XmlMessageFormatter(new[] { typeof(JobsQueueMessage) });

			return new MsmqQueue (messageQueue);
		}
	}
}

