using System;
using System.Messaging;

namespace Jobs.ServicesImp
{
	/// <summary>
	/// Queue Specific for sending job queue messages.
	/// </summary>
	public class MsmqQueue : IMessageQueue
	{
		private readonly MessageQueue _queue;

		public MsmqQueue(MessageQueue queue)
		{
			if (queue == null) {
				throw new NullReferenceException ("MessageQueue is null");
			}

			_queue = queue;
		}

		public void Send (JobsQueueMessage msg)
		{
			var message = new System.Messaging.Message { Recoverable = true, Body = msg };
			_queue.Send(message);
		}

		public JobsQueueMessage Recieve (int timeOutSeconds)
		{
			var message = _queue.Receive(new TimeSpan(0,0,timeOutSeconds));

			if (message != null)
			{
				var jobMessage = message.Body as JobsQueueMessage;

				return jobMessage;
			}

			return null;
		}
	}
}

