﻿namespace Jobs
{
    public interface IParameters
    {
        string Encode();
        void Decode(string opts);
    }
}